const socket = new WebSocket('ws://localhost:3300');

function sendMessage(e) {
	e.preventDefault();
	const input = document.getElementById('mess');
	console.log('🚀 ~ sendMessage ~ :', input.value);
	if (input.value) {
		socket.send(input.value);
		input.value = '';
	}
	input.focus();
}

const myForm = document.getElementById('form');
console.log('🚀 ~ myForm:', myForm);
myForm.addEventListener('submit', sendMessage);

// Écouter les messages
socket.addEventListener('message', (event) => {
	const data = event.data;
	const li = document.createElement('li');
	li.textContent = data;
	document.getElementById('ul').appendChild(li);
});
